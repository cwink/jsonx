#include "../include/jsonx.hpp"

#include <algorithm>
#include <cmath>
#include <fstream>
#include <sstream>

namespace {
auto parse(std::istream &stream) -> jsonx::Json;

auto parse(std::istream &stream) -> jsonx::Json {
  auto begin_of_stream = stream.tellg() == 0;

  while (std::isspace(stream.peek()) != 0) {
    stream.ignore();
  }

  if (begin_of_stream && (stream.peek() != '{' && stream.peek() != '[')) {
    throw jsonx::Error("A json file must start with an object or array.");
  }

  switch (stream.peek()) {
  case '"': {
    auto string_stream = std::ostringstream();
    stream.ignore();

    while (stream.peek() != '"') {
      if (stream.peek() == '\\') {
        string_stream << static_cast<char>(stream.get());
        switch (stream.peek()) {
        case '"':
        case '\\':
        case '/':
        case 'b':
        case 'n':
        case 'r':
        case 't':
          string_stream << static_cast<char>(stream.get());
          break;
        case 'u': {
          string_stream << static_cast<char>(stream.get());

          auto string = std::string("");
          string += static_cast<char>(stream.get());
          string += static_cast<char>(stream.get());
          string += static_cast<char>(stream.get());
          string += static_cast<char>(stream.get());

          if (!std::all_of(std::cbegin(string), std::cend(string), [](const auto ch) { return std::isxdigit(ch); })) {
            throw jsonx::Error("Invalid hex character found in '" + string + "'.");
          }
        } break;
        default:
          throw jsonx::Error("Invalid escape character encountered: '" +
                             std::string(1, static_cast<char>(stream.peek())) + "'.");
        }
        continue;
      }
      string_stream << static_cast<char>(stream.get());
    }

    stream.ignore();

    return jsonx::Json(string_stream.str());
  }
  case '{': {
    auto object = jsonx::Object();
    stream.ignore();

    while (std::isspace(stream.peek()) != 0) {
      stream.ignore();
    }

    while (stream.peek() != '}') {
      auto key = parse(stream);

      if (key.type() != jsonx::Type::string) {
        throw jsonx::Error("Key is not a valid string: '" + jsonx::string(key) + "'.");
      }

      while (std::isspace(stream.peek()) != 0) {
        stream.ignore();
      }

      if (stream.peek() != ':') {
        throw jsonx::Error("Expected a ':', got a '" + std::string(1, static_cast<char>(stream.peek())) + "'.");
      }

      stream.ignore();
      object.emplace(key.string(), parse(stream));

      while (std::isspace(stream.peek()) != 0) {
        stream.ignore();
      }

      if (stream.peek() == ',') {
        stream.ignore();
      }

      while (std::isspace(stream.peek()) != 0) {
        stream.ignore();
      }
    }

    stream.ignore();

    auto check_comma = stream.get();

    if (check_comma == ',' && stream.peek() == -1) {
      throw jsonx::Error("An extra comma was found after the end of array.");
    }

    stream.unget();

    return jsonx::Json(object);
  }
  case '[': {
    auto array = jsonx::Array();
    stream.ignore();

    while (std::isspace(stream.peek()) != 0) {
      stream.ignore();
    }

    while (stream.peek() != ']') {
      array.emplace_back(parse(stream));

      while (std::isspace(stream.peek()) != 0) {
        stream.ignore();
      }

      if (stream.peek() == ',') {
        stream.ignore();
      }

      while (std::isspace(stream.peek()) != 0) {
        stream.ignore();
      }
    }

    stream.ignore();

    auto check_comma = stream.get();

    if (check_comma == ',' && stream.peek() == -1) {
      throw jsonx::Error("An extra comma was found after the end of array.");
    }

    stream.unget();

    return jsonx::Json(array);
  }
  case '0': {
    stream.get();

    if (std::isdigit(stream.peek()) != 0) {
      throw jsonx::Error("A zero can not preceed a number.");
    }

    stream.unget();
  }
    [[fallthrough]];
  case '-':
  case '1':
  case '2':
  case '3':
  case '4':
  case '5':
  case '6':
  case '7':
  case '8':
  case '9': {
    if (stream.get() == '-' && std::isdigit(stream.peek()) == 0) {
      break;
    }

    stream.unget();
    auto number = 0.0;
    stream >> number;

    return jsonx::Json(number);
  }
  case 't':
  case 'f':
  case 'n': {
    auto string_stream = std::ostringstream();
    string_stream << static_cast<char>(stream.get());
    string_stream << static_cast<char>(stream.get());
    string_stream << static_cast<char>(stream.get());
    string_stream << static_cast<char>(stream.get());

    if (string_stream.str() == "true") {
      return jsonx::Json(true);
    }

    if (string_stream.str() == "null") {
      return jsonx::Json();
    }

    string_stream << static_cast<char>(stream.get());

    if (string_stream.str() == "false") {
      return jsonx::Json(false);
    }

    throw jsonx::Error("Invalid identifier encounted: '" + string_stream.str() + "'.");
  }
  }

  throw jsonx::Error("Invalid symbol encountered: '" + std::string(1, static_cast<char>(stream.peek())) + "'.");
}

auto string_pretty_print(const jsonx::Json &json, const size_t indent) -> std::string {
  switch (json.type()) {
  case jsonx::Type::string:
    return '"' + json.string() + '"';
  case jsonx::Type::number:
    return std::to_string(json.number());
  case jsonx::Type::boolean:
    return json.boolean() ? "true" : "false";
  case jsonx::Type::array: {
    auto string_stream = std::ostringstream();
    string_stream << '[' << std::endl;

    if (!json.array().empty()) {
      auto it = std::cbegin(json.array());
      string_stream << std::string(indent, ' ') << string_pretty_print(*it, indent + 3);
      ++it;

      for (; it != std::cend(json.array()); ++it) {
        string_stream << ',' << std::endl << std::string(indent, ' ') << string_pretty_print(*it, indent + 3);
      }
    }

    string_stream << std::endl << std::string(indent - 3, ' ') << ']';

    return string_stream.str();
  }
  case jsonx::Type::object: {
    auto string_stream = std::ostringstream();
    string_stream << '{' << std::endl;

    if (!json.object().empty()) {
      auto it = std::cbegin(json.object());
      string_stream << std::string(indent, ' ') << '"' << it->first << '"' << ':'
                    << string_pretty_print(it->second, indent + 3);
      ++it;

      for (; it != std::cend(json.object()); ++it) {
        string_stream << ',' << std::endl
                      << std::string(indent, ' ') << '"' << it->first << '"' << ':'
                      << string_pretty_print(it->second, indent + 3);
      }
    }

    string_stream << std::endl << std::string(indent - 3, ' ') << '}';

    return string_stream.str();
  }
  case jsonx::Type::null:
    return "null";
  }

  return "";
}
} // namespace

namespace jsonx {
Json::Json(String string) noexcept : string_(std::move(string)), type_{Type::string} {}

Json::Json(const Number number) noexcept : number_{number}, type_{Type::number} {}

Json::Json(const Boolean boolean) noexcept : boolean_{boolean}, type_{Type::boolean} {}

Json::Json(Array array) noexcept : array_(std::move(array)), type_{Type::array} {}

Json::Json(Object object) noexcept : object_(std::move(object)), type_{Type::object} {}

auto Json::type() const noexcept -> Type { return type_; }

auto Json::string() const -> const String & {
  if (type_ != Type::string) {
    throw Error("Json value is not of type string.");
  }

  return string_;
}

auto Json::number() const -> Number {
  if (type_ != Type::number) {
    throw Error("Json value is not of type number.");
  }

  return number_;
}

auto Json::boolean() const -> Boolean {
  if (type_ != Type::boolean) {
    throw Error("Json value is not of type boolean.");
  }

  return boolean_;
}

auto Json::array() const -> const Array & {
  if (type_ != Type::array) {
    throw Error("Json value is not of type array.");
  }

  return array_;
}

auto Json::array() -> Array & {
  if (type_ != Type::array) {
    throw Error("Json value is not of type array.");
  }

  return array_;
}

auto Json::object() const -> const Object & {
  if (type_ != Type::object) {
    throw Error("Json value is not of type object.");
  }

  return object_;
}

auto Json::object() -> Object & {
  if (type_ != Type::object) {
    throw Error("Json value is not of type object.");
  }

  return object_;
}

auto Json::string(const String &string) -> void {
  if (type_ != Type::string) {
    throw Error("Json value is not of type string.");
  }

  string_ = string;
}

auto Json::string(String &&string) -> void {
  if (type_ != Type::string) {
    throw Error("Json value is not of type string.");
  }

  string_ = std::move(string);
}

auto Json::number(const Number number) -> void {
  if (type_ != Type::number) {
    throw Error("Json value is not of type number.");
  }

  number_ = number;
}

auto Json::boolean(const Boolean boolean) -> void {
  if (type_ != Type::boolean) {
    throw Error("Json value is not of type boolean.");
  }

  boolean_ = boolean;
}

auto Json::array(const Array &array) -> void {
  if (type_ != Type::array) {
    throw Error("Json value is not of type array.");
  }

  array_ = array;
}

auto Json::array(Array &&array) -> void {
  if (type_ != Type::array) {
    throw Error("Json value is not of type array.");
  }

  array_ = std::move(array);
}

auto Json::object(const Object &object) -> void {
  if (type_ != Type::object) {
    throw Error("Json value is not of type object.");
  }

  object_ = object;
}

auto Json::object(Object &&object) -> void {
  if (type_ != Type::object) {
    throw Error("Json value is not of type object.");
  }

  object_ = std::move(object);
}

auto operator==(const Json &left, const Json &right) -> bool {
  if (left.type() != right.type()) {
    return false;
  }

  switch (left.type()) {
  case Type::string:
    return left.string() == right.string();
  case Type::number:
    return std::fabs(left.number() - right.number()) < 0.00001;
  case Type::boolean:
    return left.boolean() == right.boolean();
  case Type::array:
    if (left.array().size() != right.array().size()) {
      return false;
    }

    for (auto lit = std::cbegin(left.array()); lit != std::cend(left.array()); ++lit) {
      auto has_value = false;

      for (auto rit = std::cbegin(right.array()); rit != std::cend(right.array()); ++rit) {
        if (*lit == *rit) {
          has_value = true;
        }
      }

      if (!has_value) {
        return false;
      }
    }

    break;
  case Type::object:
    if (left.object().size() != right.object().size()) {
      return false;
    }

    for (auto lit = std::cbegin(left.object()); lit != std::cend(left.object()); ++lit) {
      auto has_value = false;

      for (auto rit = std::cbegin(right.object()); rit != std::cend(right.object()); ++rit) {
        if (lit->first == rit->first && lit->second == rit->second) {
          has_value = true;
        }
      }

      if (!has_value) {
        return false;
      }
    }

    break;
  case Type::null:
    break;
  }

  return true;
}

auto operator!=(const Json &left, const Json &right) -> bool { return !(left == right); }

auto has(const Json &json, const String &string) noexcept -> bool {
  if (json.type() == Type::object) {
    for (auto it = std::cbegin(json.object()); it != std::cend(json.object()); ++it) {
      if (it->first == string || has(it->second, string)) {
        return true;
      }
    }
  }

  return false;
}

auto get(Json &json, const String &string) noexcept -> Optional {
  if (json.type() == Type::object) {
    for (auto it = std::begin(json.object()); it != std::end(json.object()); ++it) {
      if (it->first == string) {
        return it->second;
      }

      auto value = get(it->second, string);

      if (value) {
        return value;
      }
    }
  }

  return {};
}

auto string(const Json &json) -> std::string {
  switch (json.type()) {
  case Type::string:
    return '"' + json.string() + '"';
  case Type::number:
    return std::to_string(json.number());
  case Type::boolean:
    return json.boolean() ? "true" : "false";
  case Type::array: {
    auto string_stream = std::ostringstream();
    string_stream << '[';

    if (!json.array().empty()) {
      auto it = std::cbegin(json.array());
      string_stream << string(*it);
      ++it;

      for (; it != std::cend(json.array()); ++it) {
        string_stream << ',' << string(*it);
      }
    }

    string_stream << ']';

    return string_stream.str();
  }
  case Type::object: {
    auto string_stream = std::ostringstream();
    string_stream << '{';

    if (!json.object().empty()) {
      auto it = std::cbegin(json.object());
      string_stream << '"' << it->first << '"' << ':' << string(it->second);
      ++it;

      for (; it != std::cend(json.object()); ++it) {
        string_stream << ',' << '"' << it->first << '"' << ':' << string(it->second);
      }
    }

    string_stream << '}';

    return string_stream.str();
  }
  case Type::null:
    return "null";
  }

  return "";
}

auto string_pretty_print(const Json &json) -> std::string { return ::string_pretty_print(json, 3); }

auto parse_string(const std::string &string) -> Json {
  auto stream = std::istringstream(string);
  auto json = parse(stream);

  while (std::isspace(stream.peek()) != 0) {
    stream.ignore();
  }

  if (stream.peek() != -1) {
    throw Error("Parsing finished without completing the stream. Last character was '" +
                std::string(1, static_cast<char>(stream.peek())) + "'.");
  }

  return json;
}

auto parse_file(const std::string &file) -> Json {
  auto stream = std::ifstream(file);

  if (!stream) {
    throw Error("Failed to open file '" + file + "'.");
  }

  auto json = parse(stream);

  while (std::isspace(stream.peek()) != 0) {
    stream.ignore();
  }

  if (stream.peek() != -1) {
    throw Error("Parsing finished without completing the stream. Last character was '" +
                std::string(1, static_cast<char>(stream.peek())) + "'.");
  }

  return json;
}

auto write_file(const Json &json, const std::string &file) -> void {
  auto stream = std::ofstream(file);

  if (!stream) {
    throw Error("Failed to open file '" + file + "'.");
  }

  stream << string(json);
}

auto write_file_pretty_print(const Json &json, const std::string &file) -> void {
  auto stream = std::ofstream(file);

  if (!stream) {
    throw Error("Failed to open file '" + file + "'.");
  }

  stream << string_pretty_print(json) << std::endl;
}
} // namespace jsonx
