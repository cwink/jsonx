#ifndef JSONX_HPP
#define JSONX_HPP

#include <map>
#include <memory>
#include <optional>
#include <vector>

namespace jsonx {
enum class Type : std::uint8_t { string, number, boolean, array, object, null };

using Error = std::runtime_error;

class Json;

using String = std::string;
using Number = double;
using Boolean = bool;
using Array = std::vector<Json>;
using Object = std::map<String, Json>;
using Optional = std::optional<std::reference_wrapper<Json>>;

class Json final {
  Number number_ = 0.0;
  String string_ = "";
  Array array_ = Array();
  Object object_ = Object();
  Boolean boolean_ = false;
  Type type_ = Type::null;
  [[maybe_unused]] std::uint8_t padding[6] = {0};

public:
  Json() = default;
  Json(const Json &) = default;
  Json(Json &&) noexcept = default;
  auto operator=(const Json &) -> Json & = default;
  auto operator=(Json &&) noexcept -> Json & = default;
  ~Json() noexcept = default;

  explicit Json(String string) noexcept;
  explicit Json(Number number) noexcept;
  explicit Json(Boolean boolean) noexcept;
  explicit Json(Array array) noexcept;
  explicit Json(Object object) noexcept;

  auto type() const noexcept -> Type;
  auto string() const -> const String &;
  auto number() const -> Number;
  auto boolean() const -> Boolean;
  auto array() const -> const Array &;
  auto array() -> Array &;
  auto object() const -> const Object &;
  auto object() -> Object &;

  auto string(const String &string) -> void;
  auto string(String &&string) -> void;
  auto number(Number number) -> void;
  auto boolean(Boolean boolean) -> void;
  auto array(const Array &array) -> void;
  auto array(Array &&array) -> void;
  auto object(const Object &object) -> void;
  auto object(Object &&object) -> void;
};

auto operator==(const Json &left, const Json &right) -> bool;
auto operator!=(const Json &left, const Json &right) -> bool;

auto has(const Json &json, const String &string) noexcept -> bool;
auto get(Json &json, const String &string) noexcept -> Optional;
auto string(const Json &json) -> std::string;
auto string_pretty_print(const Json &json) -> std::string;
auto parse_string(const std::string &string) -> Json;
auto parse_file(const std::string &file) -> Json;
auto write_file(const Json &json, const std::string &file) -> void;
auto write_file_pretty_print(const Json &json, const std::string &file) -> void;
} // namespace jsonx

#endif // JSONX_HPP
