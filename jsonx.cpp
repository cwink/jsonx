#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "include/jsonx.hpp"

#include <cmath>
#include <iostream>

#define TEST_READ_FILE(NAME, FILENAME, IS_TRUE)                                                                        \
  SCENARIO("When the json file " #FILENAME " is parsed, we check if it was successful or not.",                        \
           "[parsing_" #NAME "_file]") {                                                                               \
    GIVEN("The result of the parsed " #FILENAME " file.") {                                                            \
      auto parse_successful = true;                                                                                    \
                                                                                                                       \
      try {                                                                                                            \
        auto json_a = jsonx::parse_file("assets/" #FILENAME);                                                          \
      } catch (const std::runtime_error &) {                                                                           \
        parse_successful = false;                                                                                      \
      }                                                                                                                \
                                                                                                                       \
      WHEN("The result is checked.") {                                                                                 \
        THEN("The " #FILENAME " file was parsed as " #IS_TRUE ".") { REQUIRE(parse_successful == (IS_TRUE)); }         \
      }                                                                                                                \
    }                                                                                                                  \
  }

auto mock_json_object_a() -> jsonx::Json;
auto mock_json_object_b() -> jsonx::Json;

constexpr auto mock_json_object_a_str =
    R"({"a":"loldeedoe","b":9.200000,"c":true,"d":["kek",1.100000,false],"e":{"one":"bah","three":false,"two":-10.870000}})";

constexpr auto mock_json_object_a_str_pretty_print = R"({
   "a":"loldeedoe",
   "b":9.200000,
   "c":true,
   "d":[
      "kek",
      1.100000,
      false
   ],
   "e":{
      "one":"bah",
      "three":false,
      "two":-10.870000
   }
})";

auto mock_json_object_a() -> jsonx::Json {
  auto object = jsonx::Object();
  object.emplace("a", jsonx::Json(std::string("loldeedoe")));
  object.emplace("b", jsonx::Json(9.2));
  object.emplace("c", jsonx::Json(true));

  auto array = jsonx::Array();
  array.emplace_back(jsonx::Json(std::string("kek")));
  array.emplace_back(jsonx::Json(1.1));
  array.emplace_back(jsonx::Json(false));

  object.emplace("d", jsonx::Json(array));

  auto second_object = jsonx::Object();
  second_object.emplace("one", jsonx::Json(std::string("bah")));
  second_object.emplace("two", jsonx::Json(-10.87));
  second_object.emplace("three", jsonx::Json(false));

  object.emplace("e", second_object);

  return jsonx::Json(object);
}

auto mock_json_object_b() -> jsonx::Json {
  auto object = jsonx::Object();
  object.emplace("fl", jsonx::Json(std::string("heehee")));
  object.emplace("ab", jsonx::Json(1.3));
  object.emplace("ak", jsonx::Json(false));

  auto array = jsonx::Array();
  array.emplace_back(jsonx::Json(std::string("the little")));
  array.emplace_back(jsonx::Json(9.3));
  array.emplace_back(jsonx::Json(true));

  object.emplace("ca", jsonx::Json(array));

  auto second_object = jsonx::Object();
  second_object.emplace("a", jsonx::Json(std::string("humbag")));
  second_object.emplace("b", jsonx::Json(1.7));
  second_object.emplace("c", jsonx::Json(true));

  object.emplace("md", second_object);

  return jsonx::Json(object);
}

SCENARIO("When parsing a json string, the mocked object will match what is parsed.", "[parse_is_same]") {
  GIVEN("A mocked json object created from a string representation.") {
    auto json_a = jsonx::parse_string(mock_json_object_a_str);

    WHEN("The mocked json object is compared to the parsed json string.") {
      auto compare = json_a == mock_json_object_a();

      THEN("The two json objects are equal.") { REQUIRE(compare == true); }
    }
  }
}

SCENARIO("When checking a json object if it has a value for a given key, it will return the proper boolean value.",
         "[checking_has]") {
  GIVEN("A mocked json object A is created.") {
    auto json_a = mock_json_object_b();

    WHEN("We attempt to find the value 'fl'.") {
      auto value_fl = jsonx::has(json_a, "fl");

      THEN("The value returned is true as it exists.") { REQUIRE(value_fl == true); }
    }

    WHEN("We attempt to find the value 'a'.") {
      auto value_a = jsonx::has(json_a, "a");

      THEN("The value returned is true as it exists.") { REQUIRE(value_a == true); }
    }

    WHEN("We attempt to find the value 'foo'.") {
      auto value_foo = jsonx::has(json_a, "foo");

      THEN("The value returned is false as it does not exist.") { REQUIRE(value_foo == false); }
    }
  }
}

SCENARIO("When converting a json object to a string, the resulted string is correct.", "[check_string_output]") {
  GIVEN("A mocked json object A (converted to a string) and its string equivalent.") {
    auto string_a = jsonx::string(mock_json_object_a());
    auto string_b = jsonx::string_pretty_print(mock_json_object_a());

    WHEN("The two resulted strings are compared.") {
      auto compare_a = string_a == mock_json_object_a_str;
      auto compare_b = string_b == mock_json_object_a_str_pretty_print;

      THEN("The strings are equal.") {
        REQUIRE(compare_a == true);
        REQUIRE(compare_b == true);
      }
    }
  }
}

SCENARIO("When checking two json objects for equality, the result is either true or false.") {
  GIVEN("Three json objects, A, B, and C.") {
    auto json_a = mock_json_object_a();
    auto json_b = mock_json_object_a();
    auto json_c = mock_json_object_b();

    WHEN("Json object A is checked for equality with B.") {
      THEN("They are equal.") { REQUIRE(json_a == json_b); }
    }

    WHEN("Json object A is checked for equality with C.") {
      THEN("They are not equal.") { REQUIRE(json_a != json_c); }
    }
  }
}

TEST_READ_FILE(all_unicode, all_unicode.json, true)
TEST_READ_FILE(blns, blns.json, true)
TEST_READ_FILE(canada, canada.json, true)
TEST_READ_FILE(fail10, fail10.json, false)
TEST_READ_FILE(fail11, fail11.json, false)
TEST_READ_FILE(fail12, fail12.json, false)
TEST_READ_FILE(fail13, fail13.json, false)
TEST_READ_FILE(fail14, fail14.json, false)
TEST_READ_FILE(fail15, fail15.json, false)
TEST_READ_FILE(fail16, fail16.json, false)
TEST_READ_FILE(fail17, fail17.json, false)
TEST_READ_FILE(fail19, fail19.json, false)
TEST_READ_FILE(fail1, fail1.json, false)
TEST_READ_FILE(fail20, fail20.json, false)
TEST_READ_FILE(fail21, fail21.json, false)
TEST_READ_FILE(fail22, fail22.json, false)
TEST_READ_FILE(fail23, fail23.json, false)
TEST_READ_FILE(fail24, fail24.json, false)
TEST_READ_FILE(fail26, fail26.json, false)
TEST_READ_FILE(fail28, fail28.json, false)
TEST_READ_FILE(fail29, fail29.json, false)
TEST_READ_FILE(fail2, fail2.json, false)
TEST_READ_FILE(fail30, fail30.json, false)
TEST_READ_FILE(fail31, fail31.json, false)
TEST_READ_FILE(fail32, fail32.json, false)
TEST_READ_FILE(fail33, fail33.json, false)
TEST_READ_FILE(fail3, fail3.json, false)
TEST_READ_FILE(fail5, fail5.json, false)
TEST_READ_FILE(fail6, fail6.json, false)
TEST_READ_FILE(fail7, fail7.json, false)
TEST_READ_FILE(fail8, fail8.json, false)
TEST_READ_FILE(json_org_1, json_org_1.json, true)
TEST_READ_FILE(json_org_2, json_org_2.json, true)
TEST_READ_FILE(json_org_3, json_org_3.json, true)
TEST_READ_FILE(json_org_4, json_org_4.json, true)
TEST_READ_FILE(json_org_5, json_org_5.json, true)
TEST_READ_FILE(pass1, pass1.json, true)
TEST_READ_FILE(pass2, pass2.json, true)
TEST_READ_FILE(pass3, pass3.json, true)

SCENARIO("When attempting to obtain a json object for a given key, the result either exists or doesn't.",
         "[check_get_function]") {
  GIVEN("Three json get attempts, B, C, and D.") {
    auto json_a = mock_json_object_a();
    auto json_b = jsonx::get(json_a, "a");
    auto json_c = jsonx::get(json_a, "two");
    auto json_d = jsonx::get(json_a, "lol");

    WHEN("B is checked.") {
      REQUIRE(json_b.has_value() == true);

      THEN("B has the value 'loldeedoe'.") { REQUIRE(json_b->get().string() == "loldeedoe"); }
    }

    WHEN("C is checked.") {
      REQUIRE(json_c.has_value() == true);

      THEN("C has the value -10.87.") { REQUIRE(std::fabs(json_c->get().number() - -10.87) < 0.00001); }
    }

    WHEN("D is checked.") {
      THEN("D does not exist.") { REQUIRE(json_d.has_value() == false); }
    }
  }
}

SCENARIO("When attempting to write a mocked json object, it not only writes, but can also be read back and compared.",
         "[write_file]") {
  GIVEN("A mocked json object.") {
    auto json_a = mock_json_object_a();

    WHEN("The object is written to a file.") {
      jsonx::write_file(json_a, "write_file.json");
      jsonx::write_file_pretty_print(json_a, "write_file_pretty_print.json");

      THEN("It is read back again and compared with the original object.") {
        auto json_b = jsonx::parse_file("write_file.json");
        auto json_c = jsonx::parse_file("write_file_pretty_print.json");

        REQUIRE(json_a == json_b);
        REQUIRE(json_a == json_c);
      }
    }
  }
}
